﻿namespace Carrefour.Teste.Domain.Enum
{
    public enum TipoEnderecoEnum
    {
        EnderecoResidencial = 1,
        EnderecoComercial = 2,
        Outros = 3
    }
}
