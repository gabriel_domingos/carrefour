﻿using System.Threading.Tasks;

namespace Carrefour.Teste.Domain
{
    public interface IClienteEntityFrameworkRepository
    {
        Task<Cliente> addClienteAsync(Cliente cliente);
        Task<Cliente> getClienteById(int id);
        Task<Cliente> getClienteByCPFAsync(string cpf);
        Task updateClienteAsync(Cliente cliente);
        Task deleteClienteAsync(int idCliente);
    }
}
