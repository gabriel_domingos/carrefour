﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Carrefour.Teste.Domain
{
    public interface IClienteDapperRepository
    {
         Task<List<Cliente>> clientesAsync(int codEmpresa, string nome, string cpf, string cidade, string estado);
    }
}
