﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Carrefour.Teste.Domain
{
    public interface IClienteService
    {
        Task<Cliente> addClienteAsync(Cliente cliente);
        Task<Cliente> getClienteByIdAsync(int id);
        Task<Cliente> getClienteByCPFAsync(string cpf);
        Task updateClienteAsync(Cliente cliente);
        Task deleteClienteAsync(int idCliente);
        Task<List<Cliente>> getTodosClientesAsync(int codEmpresa, string nome, string cpf, string cidade, string estado);
    }
}
