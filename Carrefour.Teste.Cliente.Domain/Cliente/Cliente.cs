﻿using Carrefour.Teste.Domain.Endereco;
using System;
using System.Collections.Generic;

namespace Carrefour.Teste.Domain
{
    public class Cliente
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string rg { get; set; }
        public string cpf { get; set; }
        public DateTime data_nascimento { get; set; }
        public string telefone { get; set; }
        public string email { get; set; }
        public int cod_empresa { get; set; }
        public List<ClienteEndereco> clienteEnderecos { get; set; }

        public Cliente()
        {

        }
        public void setUpdate(string _nome, string _rg, DateTime _data_nascimento, string  _telefone, string _email)
        {
            nome = _nome;
            rg = _rg;
            data_nascimento = _data_nascimento;
            telefone = _telefone;
            email = _email;
        }
    }
}
