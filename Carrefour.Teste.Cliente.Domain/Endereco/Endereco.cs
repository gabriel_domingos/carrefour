﻿using System.Collections.Generic;

namespace Carrefour.Teste.Domain.Endereco
{
    public class Endereco
    {
        public int id { get; set; }
        public string rua { get; set; }
        public string bairro { get; set; }
        public string numero { get; set; }
        public int id_cidade { get; set; }
        public string complemento { get; set; }
        public string cep { get; set; }
        public int tipo_endereco { get; set; }

    }
}
