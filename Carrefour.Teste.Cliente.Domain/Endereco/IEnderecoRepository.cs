﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Carrefour.Teste.Domain.Endereco
{
    public interface IEnderecoRepository
    {
        Task<Endereco> addAsync(Endereco endereco);
        Task<ClienteEndereco> addClienteEndereco(ClienteEndereco clienteEndereco);
        Task deleteEndereco(List<Endereco> enderecos);
        Task deleteClienteEndereco(List<ClienteEndereco> clienteEnderecos);
    }
}
