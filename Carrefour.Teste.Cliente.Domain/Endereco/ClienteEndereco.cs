﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Carrefour.Teste.Domain.Endereco
{
    public class ClienteEndereco
    {
        public int id { get; set; }

        [ForeignKey("Cliente")]
        [Column("tb_cliente_id")]
        public int Clienteid { get; set; }
        [ForeignKey("Endereco")]
        [Column("tb_endereco_id")]
        public int Enderecoid { get; set; }
        public virtual Endereco  Endereco { get; set; }
    }
}
