﻿using System.Collections.Generic;

namespace Carrefour.Teste.Domain.Endereco
{
    public interface IEnderecoService
    {
        void addEndereco(List<Endereco> enderecos, int idCliente);
    }
}
