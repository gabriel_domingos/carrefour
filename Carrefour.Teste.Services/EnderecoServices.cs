﻿using Carrefour.Teste.Domain.Endereco;
using System.Collections.Generic;

namespace Carrefour.Teste.Services
{
    public class EnderecoServices: IEnderecoService
    {
        private readonly IEnderecoRepository _enderecoRepository;

        public EnderecoServices(IEnderecoRepository enderecoRepository)
        {
            _enderecoRepository = enderecoRepository;
        }

        public async void addEndereco(List<Endereco> enderecos, int idCliente)
        {
            foreach(var endereco in enderecos)
            {
                var resultEndereco = await _enderecoRepository.addAsync(endereco);
                await _enderecoRepository.addClienteEndereco(new ClienteEndereco()
                {
                    Clienteid = idCliente,
                    Enderecoid = resultEndereco.id
                });
            }
        }
    }
}
