﻿using Carrefour.Teste.Domain;
using Carrefour.Teste.Domain.Endereco;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Carrefour.Teste.Services
{
    public class ClienteServices : IClienteService
    {
        private readonly IClienteEntityFrameworkRepository _clienteEntityFrameworkRepository;
        private readonly IEnderecoRepository _enderecoRepository;
        private readonly IClienteDapperRepository _clienteDapperRepository;
        public ClienteServices(
            IClienteEntityFrameworkRepository clienteEntityFrameworkRepository, 
            IEnderecoRepository enderecoRepository,
            IClienteDapperRepository clienteDapperRepository 
         )
        {
            _clienteEntityFrameworkRepository = clienteEntityFrameworkRepository;
            _enderecoRepository = enderecoRepository;
             _clienteDapperRepository = clienteDapperRepository;
        }

        public async Task<Cliente> addClienteAsync(Cliente cliente)
            => await _clienteEntityFrameworkRepository
                     .addClienteAsync(cliente);

        public async Task deleteClienteAsync(int idCliente)
        {
            var cliente = await _clienteEntityFrameworkRepository.getClienteById(idCliente);
            var enderecos = new List<Endereco>();

            cliente.clienteEnderecos.ForEach(cliEnd => 
                    enderecos.Add(cliEnd.Endereco)
            );

             await  _enderecoRepository.deleteClienteEndereco(cliente.clienteEnderecos);
             await _enderecoRepository.deleteEndereco(enderecos);
             await _clienteEntityFrameworkRepository.deleteClienteAsync(idCliente);
        }

        public async Task<Cliente> getClienteByCPFAsync(string cpf)
            => await _clienteEntityFrameworkRepository
                      .getClienteByCPFAsync(cpf);

        public async Task<Cliente> getClienteByIdAsync(int id)
            => await _clienteEntityFrameworkRepository
                     .getClienteById(id);

        public async Task<List<Cliente>> getTodosClientesAsync(int codEmpresa, string nome, string cpf, string cidade, string estado)
            => await _clienteDapperRepository
                    .clientesAsync(codEmpresa, nome, cpf, cidade, estado);

        public async Task updateClienteAsync(Cliente cliente)
            => await _clienteEntityFrameworkRepository
                    .updateClienteAsync(cliente);
    }
}
