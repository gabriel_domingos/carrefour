﻿using Carrefour.Teste.Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Carrefour.Teste.EntityFramework.Repository
{
    public class ClienteRepository : IClienteEntityFrameworkRepository
    {
        public async Task<Cliente> addClienteAsync(Cliente cliente)
        {

            using (var context = new ClienteContext())
            {
                context.Clientes.Add(cliente);
                await context.SaveChangesAsync();

                return cliente;
            }
        }

        public async Task deleteClienteAsync(int idCliente)
        {
            using (var context = new ClienteContext())
            {
                var cliente = context.Clientes.Find(idCliente);
                context.Clientes.Remove(cliente);
                await context.SaveChangesAsync();
            }
        }

        public async Task<Cliente> getClienteByCPFAsync(string cpf)
        {
            using (var context = new ClienteContext())
            {

                return await context
                             .Clientes
                             .Where(cli => cli.cpf == cpf)
                             .FirstOrDefaultAsync();
            }
        }

        public async Task<Cliente> getClienteById(int id)
        {
            using (var context = new ClienteContext())
            {

                return await context
                             .Clientes
                             .Include(x => x.clienteEnderecos)
                             .Where(cli => cli.id == id)
                             .Select(x => new Cliente
                             {
                                 nome = x.nome,
                                 cpf = x.cpf,
                                 rg = x.rg,
                                 data_nascimento = x.data_nascimento,
                                 email = x.email,
                                 telefone = x.telefone,
                                 id = x.id,
                                 cod_empresa = x.cod_empresa,
                                 clienteEnderecos = x.clienteEnderecos
                                    .Join(context.Enderecos, e => e.Enderecoid, c => c.id, (e, c) => new { cliEnd = e, end = c })
                                    .Select(j => new Domain.Endereco.ClienteEndereco
                                    {
                                        id = j.cliEnd.id,
                                        Enderecoid = j.cliEnd.Enderecoid,
                                        Endereco = j.end
                                    }).ToList()
                             })
                             .FirstOrDefaultAsync();
            }
        }

        public async Task updateClienteAsync(Cliente cliente)
        {
            using (var context = new ClienteContext())
            {
                context.Clientes.Update(cliente);
               await  context.SaveChangesAsync();
            }
        }
    }

}

