﻿using Carrefour.Teste.Domain.Endereco;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Carrefour.Teste.EntityFramework.Repository
{
    public class EnderecoRepository : IEnderecoRepository
    {
        public async Task<Endereco> addAsync(Endereco endereco)
        {
            using (var context = new  ClienteContext())
            {
                context.Enderecos.Add(endereco);
                await context.SaveChangesAsync();
                return endereco;
            }
        }

        public async Task<ClienteEndereco> addClienteEndereco(ClienteEndereco clienteEndereco)
        {
            using (var context = new ClienteContext())
            {
                context.ClienteEnderecos.Add(clienteEndereco);
                await context.SaveChangesAsync();
                return clienteEndereco;
            }
        }

        public async Task deleteClienteEndereco(List<ClienteEndereco> clienteEnderecos)
        {
            using (var context = new ClienteContext())
            {
                context.ClienteEnderecos.RemoveRange(clienteEnderecos);
                await context.SaveChangesAsync();
            }
        }

        public async Task deleteEndereco(List<Endereco> enderecos)
        {
            using (var context = new ClienteContext())
            {
                context.Enderecos.RemoveRange(enderecos);
                await context.SaveChangesAsync();
            }
        }
    }
}
