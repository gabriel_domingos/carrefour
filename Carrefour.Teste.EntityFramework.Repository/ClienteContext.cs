﻿using Carrefour.Teste.Util;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Carrefour.Teste.Domain;
using Carrefour.Teste.Domain.Endereco;

namespace Carrefour.Teste.EntityFramework.Repository
{
    public class ClienteContext:DbContext
    {
        private readonly IConfigurationRoot _configuration;
        public ClienteContext()
        {
            _configuration = new Configuration()
                                    .configuration;

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Cliente>(entity => {
                entity.ToTable("TB_CLIENTE");
            });

            builder.Entity<Endereco>(entity => {
                entity.ToTable("TB_ENDERECO");
            });

            builder.Entity<ClienteEndereco>(entity => {
                entity.ToTable("TB_CLIENTE_ENDERECO");
            });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlServer(
                    _configuration
                    .GetConnectionString("bdCarrefourCliente")
                );
        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Endereco> Enderecos { get; set; }
        public DbSet<ClienteEndereco> ClienteEnderecos { get; set; }

    }
}
