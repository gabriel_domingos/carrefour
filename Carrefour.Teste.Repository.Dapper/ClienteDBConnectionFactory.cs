﻿using Carrefour.Teste.Dapper.Repository.Interface;
using Carrefour.Teste.Util;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;

namespace Carrefour.Teste.Repository.Dapper
{
    public class ClienteDBConnectionFactory : IConnectionFactory
    {
        private readonly IConfigurationRoot _configuration;
    
        public ClienteDBConnectionFactory()
        {
            _configuration = new Configuration()
                                    .configuration;

        }
        public IDbConnection Connection()
        {
            return new SqlConnection(_configuration.GetConnectionString("bdCarrefourCliente"));
        }
    }
}
