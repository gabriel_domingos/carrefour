﻿using System.Data;

namespace Carrefour.Teste.Dapper.Repository.Interface
{
    public interface IConnectionFactory
    {
        IDbConnection Connection();
    }
}
