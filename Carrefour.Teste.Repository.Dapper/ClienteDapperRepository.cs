﻿using Carrefour.Teste.Dapper.Repository.Interface;
using Carrefour.Teste.Domain;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Carrefour.Teste.Dapper.Repository
{
    public class ClienteDapperRepository : IClienteDapperRepository
    {
        private readonly IConnectionFactory _connection;
        public ClienteDapperRepository(IConnectionFactory connectionFactory)
        {
            _connection = connectionFactory;
        }
        public async Task<List<Cliente>> clientesAsync(int codEmpresa, string nome, string cpf, string cidade, string estado)
        {

            using (var connectionDb = _connection.Connection())
            {
				string query = @"
					SELECT 
						cli.id,
						cli.nome,
						rg,
						cpf,
						data_nascimento, 
						telefone,
						email,
						cod_empresa 
					FROM tb_cliente cli, tb_cidade cidade, tb_endereco endereco, tb_cliente_endereco cliEnd
					WHERE cliEnd.tb_cliente_id = cli.id
						AND cliEnd.tb_endereco_id = endereco.id
						AND cidade.id = endereco.id_cidade
						AND cli.cod_empresa = @codEmpresa
						AND cli.nome  = ISNULL(@nome, cli.nome)
						AND cli.cpf = ISNULL(@cpf, cli.cpf)
						AND cidade.nome = ISNULL(@cidade, cidade.nome)
						AND cidade.estado = ISNULL(@estado, cidade.estado)
					group by
						cli.id,
						cli.nome,
						rg,
						cpf,
						data_nascimento, 
						telefone,
						email,
						cod_empresa";

				var parameters = new DynamicParameters();
				parameters.Add(name: "codEmpresa", dbType: DbType.Int32, direction: ParameterDirection.Input, value: codEmpresa);
				
				if (!string.IsNullOrEmpty(nome))
					parameters.Add(name: "nome", dbType: DbType.String, direction: ParameterDirection.Input, value: nome);
				else
					parameters.Add(name: "nome", dbType: DbType.String, direction: ParameterDirection.Input, value: DBNull.Value);

				if (!string.IsNullOrEmpty(cpf))
					parameters.Add(name: "cpf", dbType: DbType.String, direction: ParameterDirection.Input, value: cpf);
				else
					parameters.Add(name: "cpf", dbType: DbType.String, direction: ParameterDirection.Input, value: DBNull.Value);

				if (!string.IsNullOrEmpty(cidade))
					parameters.Add(name: "cidade", dbType: DbType.String, direction: ParameterDirection.Input, value: cidade);
				else
					parameters.Add(name: "cidade", dbType: DbType.String, direction: ParameterDirection.Input, value: DBNull.Value);

				if (!string.IsNullOrEmpty(estado))
					parameters.Add(name: "estado", dbType: DbType.String, direction: ParameterDirection.Input, value: estado);
				else
					parameters.Add(name: "estado", dbType: DbType.String, direction: ParameterDirection.Input, value: DBNull.Value);


				var resultClientes = await connectionDb.QueryAsync<Cliente>(query, parameters);

				return resultClientes.AsList();
			}
        }
    }
}
