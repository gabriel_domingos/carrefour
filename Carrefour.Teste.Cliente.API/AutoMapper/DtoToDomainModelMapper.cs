﻿using AutoMapper;
using Carrefour.Teste.Cliente.API.DTO;
using Carrefour.Teste.Domain.Endereco;

namespace Carrefour.Teste.Cliente.API.AutoMapper
{
    public class DtoToDomainModelMapper: Profile
    {
        public DtoToDomainModelMapper()
        {
            CreateMap<AddClienteDTO, Carrefour.Teste.Domain.Cliente>();
            CreateMap<AddEnderecoDTO, Endereco>();
        }
    }
}
