﻿using AutoMapper;
using Carrefour.Teste.Cliente.API.DTO;
using Carrefour.Teste.Domain.Endereco;

namespace Carrefour.Teste.Cliente.API.AutoMapper
{
    public class DomainModelToDtoMapper: Profile
    {
        public DomainModelToDtoMapper()
        {
            CreateMap<Carrefour.Teste.Domain.Cliente, AddClienteDTO>();
            CreateMap<Endereco, AddEnderecoDTO>();
        }
      
    }
}
