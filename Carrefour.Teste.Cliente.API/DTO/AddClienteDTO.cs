﻿using Carrefour.Teste.Cliente.API.Validate;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Carrefour.Teste.Cliente.API.DTO
{
    public class AddClienteDTO
    {
        [Required(ErrorMessage = "Nome Obrigatório", AllowEmptyStrings = false)]
        [StringLength(150, ErrorMessage = "Tamanho máximo 150 caracteres")]
        public string nome { get; set; }
        [Required(ErrorMessage = "RG Obrigatório", AllowEmptyStrings = false)]
        [StringLength(20, ErrorMessage = "Tamanho máximo 20 caracteres")]
        public string rg { get; set; }

        [CPFValidate]
        [StringLength(20, ErrorMessage = "Tamanho máximo 20 caracteres")]
        public string cpf { get; set; }
        [Required(ErrorMessage = "Data Nascimento Obrigatória", AllowEmptyStrings = false)]
        [DataType(DataType.Date)]
        public DateTime data_nascimento { get; set; }

        [StringLength(20, ErrorMessage = "Tamanho máximo 20 caracteres")]
        public string telefone { get; set; }


        [StringLength(20, ErrorMessage = "Tamanho máximo 20 caracteres")]
        public string email { get; set; }

        [ValidaEmpresa]
        public int cod_empresa { get; set; }
        [MinLength(1, ErrorMessage = "Endereço Obrigatório")]
        [MesmoTipoEnderecoValidate]
        public List<AddEnderecoDTO> enderecos { get; set; }

    }

    public class AddEnderecoDTO
    {
        [Required(ErrorMessage = "Rua Obrigatório", AllowEmptyStrings = false)]
        [StringLength(255, ErrorMessage = "Tamanho máximo 255 caracteres")]
        public string rua { get; set; }
        [Required(ErrorMessage = "Bairro Obrigatório", AllowEmptyStrings = false)]
        [StringLength(50, ErrorMessage = "Tamanho máximo 50 caracteres")]
        public string bairro { get; set; }
        [Required(ErrorMessage = "Numero Obrigatório", AllowEmptyStrings = false)]
        [StringLength(50, ErrorMessage = "Tamanho máximo 50 caracteres")]
        public string numero { get; set; }
        [Required]
        public int id_cidade { get; set; }
       
        public string complemento { get; set; }
        [Required(ErrorMessage = "Cep Obrigatório", AllowEmptyStrings = false)]
        [StringLength(255, ErrorMessage = "Tamanho máximo 10 caracteres")]
        public string cep { get; set; }
        [Required]
        [TipoEnderecoValidate]
        public int tipo_endereco { get; set; }
    }
}
