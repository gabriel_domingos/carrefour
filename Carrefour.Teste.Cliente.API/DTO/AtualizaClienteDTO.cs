﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Carrefour.Teste.Cliente.API.DTO
{
    public class AtualizaClienteDTO
    {


        [Required(ErrorMessage = "Nome Obrigatório", AllowEmptyStrings = false)]
        [StringLength(150, ErrorMessage = "Tamanho máximo 150 caracteres")]
        public string nome { get; set; }
        [Required(ErrorMessage = "RG Obrigatório", AllowEmptyStrings = false)]
        [StringLength(20, ErrorMessage = "Tamanho máximo 20 caracteres")]
        public string rg { get; set; }

        [Required(ErrorMessage = "Data Nascimento Obrigatória", AllowEmptyStrings = false)]
        [DataType(DataType.Date)]
        public DateTime data_nascimento { get; set; }

        [StringLength(20, ErrorMessage = "Tamanho máximo 20 caracteres")]
        public string telefone { get; set; }

        [StringLength(20, ErrorMessage = "Tamanho máximo 20 caracteres")]
        public string email { get; set; }
    }
}
