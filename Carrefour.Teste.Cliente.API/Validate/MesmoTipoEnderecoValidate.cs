﻿using Carrefour.Teste.Cliente.API.DTO;
using Carrefour.Teste.Domain.Enum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Carrefour.Teste.Cliente.API.Validate
{
    public class MesmoTipoEnderecoValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var enderecos = value as List<AddEnderecoDTO>;
            if (enderecos.Where(e => e.tipo_endereco == (int)TipoEnderecoEnum.EnderecoComercial).Count() > 1)
                return new ValidationResult("Adicione apenas 1 endereço Comercial");
            else if (enderecos.Where(e => e.tipo_endereco == (int)TipoEnderecoEnum.EnderecoResidencial).Count() > 1)
                return new ValidationResult("Adicione apenas 1 endereço Residencial");
            else if (enderecos.Where(e => e.tipo_endereco == (int)TipoEnderecoEnum.EnderecoResidencial).Count() > 1)
                return new ValidationResult("Adicione apenas 1 outro endereço");
            else
                return ValidationResult.Success;
        }
    }
}
