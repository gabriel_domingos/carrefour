﻿using Carrefour.Teste.Cliente.API.Util;
using System;
using System.ComponentModel.DataAnnotations;

namespace Carrefour.Teste.Cliente.API.Validate
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class CPFValidate: ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if (value is null || !CPFUtil.ValidaCPF((string)value) || string.IsNullOrEmpty((string)value))
            {
                return new ValidationResult("CPF Inválido");
            }

            return ValidationResult.Success;
        }
    }
}
