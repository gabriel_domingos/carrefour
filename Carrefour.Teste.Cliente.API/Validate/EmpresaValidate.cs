﻿using Carrefour.Teste.Domain.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace Carrefour.Teste.Cliente.API.Validate
{
    [AttributeUsageAttribute(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class ValidaEmpresa : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if (value is null ||  ((int)value != (int)EmpresaEnum.Carrefour && (int)value != (int)EmpresaEnum.Atacadao))
            {
                return new ValidationResult("Código da Empresa Inválido");
            }
            return ValidationResult.Success;
        }
    }
}
