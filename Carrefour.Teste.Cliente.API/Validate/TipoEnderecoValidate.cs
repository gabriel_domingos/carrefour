﻿using Carrefour.Teste.Domain.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace Carrefour.Teste.Cliente.API.Validate
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class TipoEnderecoValidate: ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if (value is null || ((int)value != (int)TipoEnderecoEnum.EnderecoComercial && (int)value != (int)TipoEnderecoEnum.EnderecoResidencial && (int)value != (int)TipoEnderecoEnum.Outros))
            {
                return new ValidationResult("Tipo de Endereço Inválido");
            }
            return ValidationResult.Success;
        }
    }
}
