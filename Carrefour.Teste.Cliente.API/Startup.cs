using AutoMapper;
using Carrefour.Teste.Cliente.API.AutoMapper;
using Carrefour.Teste.Dapper.Repository;
using Carrefour.Teste.Dapper.Repository.Interface;
using Carrefour.Teste.Domain;
using Carrefour.Teste.Domain.Endereco;
using Carrefour.Teste.EntityFramework.Repository;
using Carrefour.Teste.Repository.Dapper;
using Carrefour.Teste.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Carrefour.Teste.Cliente.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();


            services.AddSwaggerGen(c => {

                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Teste Carrefour", Version = "v1", Contact = new OpenApiContact() { Name = "Gabriel" } });
            });

            services.AddScoped<IConnectionFactory, ClienteDBConnectionFactory>();
            services.AddScoped<IClienteEntityFrameworkRepository, ClienteRepository>();
            services.AddScoped<IClienteDapperRepository, ClienteDapperRepository>();
            services.AddTransient<IClienteService, ClienteServices>();

            services.AddScoped<IEnderecoRepository, EnderecoRepository>();
            services.AddTransient<IEnderecoService, EnderecoServices>();

            var mapperConfig = new MapperConfiguration((mapper) =>
            {
                mapper.AddProfile<DomainModelToDtoMapper>();
                mapper.AddProfile<DtoToDomainModelMapper>();
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API Cliente V1");
            });


            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
