﻿using AutoMapper;
using Carrefour.Teste.Cliente.API.DTO;
using Carrefour.Teste.Domain;
using Carrefour.Teste.Domain.Endereco;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Carrefour.Teste.Cliente.API.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private readonly IClienteService _clienteService;
        private readonly IEnderecoService _enderecoService;
        private readonly IMapper _mapper;

        public ClientesController(IClienteService clienteService, IMapper mapper, IEnderecoService enderecoService)
        {
            _clienteService = clienteService;
            _mapper = mapper;
            _enderecoService = enderecoService;
        }

        [HttpPost("add")]
        public async Task<IActionResult> add(AddClienteDTO addClienteDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var cliente = await _clienteService.getClienteByCPFAsync(addClienteDTO.cpf);

            if (cliente is null || cliente.cod_empresa != addClienteDTO.cod_empresa)
            {
                cliente = await _clienteService.addClienteAsync(_mapper.Map<AddClienteDTO, Carrefour.Teste.Domain.Cliente>(addClienteDTO));
                _enderecoService.addEndereco(_mapper.Map<List<AddEnderecoDTO>, List<Endereco>>(addClienteDTO.enderecos), cliente.id);

                var clienteResult = await _clienteService.getClienteByIdAsync(cliente.id);
                return Created($"cliente/{cliente.id}", clienteResult);
            }
            else
                return BadRequest(new { erro = "Cliente já cadastrado com o mesmo CPF" });


        }

        [HttpGet("{id}")]
        public async Task<IActionResult> getById(int id)
        {
            var cliente = await _clienteService.getClienteByIdAsync(id);

            if (cliente is null)
                return NotFound();

            return Ok(cliente);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> atualizarCliente(int id, AtualizaClienteDTO atualizaClienteDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var cliente = await _clienteService.getClienteByIdAsync(id);

            if (cliente is null)
                return NoContent();

            cliente.setUpdate(
                atualizaClienteDTO.nome,
                atualizaClienteDTO.rg,
                atualizaClienteDTO.data_nascimento,
                atualizaClienteDTO.telefone,
                atualizaClienteDTO.email
             );

            await _clienteService.updateClienteAsync(cliente);

            return Ok(cliente);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> deletarCliente(int id)
        {
            await _clienteService.deleteClienteAsync(id);
            return NoContent();
        }

        [HttpGet("todos")]
        public async Task<IActionResult> todos(int codEmpresa, string nome = null, string cpf = null, string cidade = null, string estado = null)
        {
            if (codEmpresa == 0)
                return BadRequest(new { erro = "Código da Empresa Obrigatório" });

            var todosClientes = await _clienteService.getTodosClientesAsync(codEmpresa, nome, cpf, cidade, estado);

            return Ok(todosClientes);
        }
    }
}
