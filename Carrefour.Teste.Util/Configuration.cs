﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Carrefour.Teste.Util
{
    public class Configuration
    {
        public IConfigurationRoot configuration;

        public Configuration()
        {

            string settingsFile = string.Empty;
            if (!(Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") is null))
            {
                var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT").ToString();
            

                if (env == "Production")
                    settingsFile = "appsettings.json";
                else if (env == "Development")
                    settingsFile = "appsettings.Development.json";
                else if (env == "Staging")
                    settingsFile = "appsettings.Staging.json";
            }else
                settingsFile = "appsettings.Development.json";

            var builder = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile(settingsFile, optional: true, reloadOnChange: false);

            configuration = builder.Build();
        }
    }
}
