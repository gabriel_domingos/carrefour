﻿using Carrefour.Teste.Cliente.API;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Priority;

namespace Carrefour.Teste.Avaliacao.Test
{
    [TestCaseOrderer(PriorityOrderer.Name, PriorityOrderer.Assembly)]
    public class ClienteTest
    {
        private readonly HttpClient _client;

        public ClienteTest()
        {
            var server = new TestServer(new WebHostBuilder()
                .UseEnvironment("Developement")
                .UseStartup<Startup>());

            _client = server.CreateClient();
        }

        
        [Fact, Priority(0)]
        public async Task CriarCliente()
        {
            var cliente = new
            {
                 nome = "Gabriel",
                 rg = "463842385",
                 cpf = "240.832.160-34",
                 data_nascimento = "1994-11-14",
                 telefone = "11991941911",
                 email = "teste@gmail.com",
                 cod_empresa = 2,
                 enderecos = new List<object>{
                     new
                     {
                       rua =  "Rua Camarão Vermelho",
                       bairro = "Portal dos Ipes 3",
                       numero = "385",
                       id_cidade = 1,
                       complemento = "",
                       cep = "07791120",
                       tipo_endereco = 2
                     }
                  }
            };
            var clienteJson = JsonConvert.SerializeObject(cliente);
            var content = new StringContent(clienteJson, Encoding.UTF8, "application/json");

      
            var response = await _client.PostAsync($"/api/v1/Clientes/add", content);

            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }
        
        
        [Theory, Priority(1)]
        [InlineData(1)]
        public async Task AtualizarCliente(int id)
        {
            var cliente = new
            {
                nome = "teste",
                rg = "463842385",
                cpf = "240.832.160-34",
                data_nascimento = "1994-11-14",
                telefone = "11991941911",
                email = "teste2@gmail.com",
            };
            var clienteJson = JsonConvert.SerializeObject(cliente);
            var content = new StringContent(clienteJson, Encoding.UTF8, "application/json");


            var response = await _client.PutAsync($"/api/v1/Clientes/{id}", content);
  
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Theory, Priority(2)]
        [InlineData("GET", 1)]
        public async Task GetClienteByIdTest(string method, int id)
        {
            var request = new HttpRequestMessage(new HttpMethod(method), $"/api/v1/Clientes/{id}");
            var response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Theory, Priority(3)]
        [InlineData("GET", 1)]
        public async Task GetTodosCientesCarrefour(string method, int codEmpresa)
        {
            var request = new HttpRequestMessage(new HttpMethod(method), $"/api/v1/Clientes/todos?codEmpresa={codEmpresa}");
            var response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Theory, Priority(4)]
        [InlineData("GET", 2)]
        public async Task GetTodosCientesAtacadao(string method, int codEmpresa)
        {
            var request = new HttpRequestMessage(new HttpMethod(method), $"/api/v1/Clientes/todos?codEmpresa={codEmpresa}");
            var response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

  


        [Theory, Priority(6)]
        [InlineData("GET")]
        public async Task ValidaBadRequestCodEmpresa(string method)
        {
            var request = new HttpRequestMessage(new HttpMethod(method), $"/api/v1/Clientes/todos");
            var response = await _client.SendAsync(request);

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Theory, Priority(7)]
        [InlineData(1)]
        public async Task DeletarCliente(int id)
        {

            var response = await _client.DeleteAsync($"/api/v1/Clientes/{id}");
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }


    }
}
