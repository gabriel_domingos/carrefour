FROM mcr.microsoft.com/dotnet/core/runtime:3.1-buster-slim AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["Carrefour.Teste.Avaliacao.Test/Carrefour.Teste.Avaliacao.Test.csproj", "Carrefour.Teste.Avaliacao.Test/"]
COPY ["Carrefour.Teste.Cliente.Domain/Carrefour.Teste.Domain.csproj", "Carrefour.Teste.Cliente.Domain/"]
COPY ["Carrefour.Teste.Cliente.API/Carrefour.Teste.Cliente.API.csproj", "Carrefour.Teste.Cliente.API/"]
COPY ["Carrefour.Teste.Repository.Dapper/Carrefour.Teste.Dapper.Repository.csproj", "Carrefour.Teste.Repository.Dapper/"]
COPY ["Carrefour.Teste.Util/Carrefour.Teste.Util.csproj", "Carrefour.Teste.Util/"]
COPY ["Carrefour.Teste.Services/Carrefour.Teste.Services.csproj", "Carrefour.Teste.Services/"]
COPY ["Carrefour.Teste.EntityFramework.Repository/Carrefour.Teste.EntityFramework.Repository.csproj", "Carrefour.Teste.EntityFramework.Repository/"]
RUN dotnet restore "Carrefour.Teste.Avaliacao.Test/Carrefour.Teste.Avaliacao.Test.csproj"
COPY . .
WORKDIR "/src/Carrefour.Teste.Avaliacao.Test"
RUN dotnet test "Carrefour.Teste.Avaliacao.Test.csproj"