
create DATABASE CARREFOUR_CLIENTES
go

USE CARREFOUR_CLIENTES

go

CREATE TABLE TB_CLIENTE(
	id int identity(1,1) primary key,
	nome varchar(150) not null,
	rg varchar(20) not null,
	cpf varchar(20) not null,
	data_nascimento date not null, 
	telefone varchar(20),
	email varchar(20),
	cod_empresa int not null
)

go

CREATE TABLE TB_CIDADE(
	id int identity(1,1) primary key,
	nome varchar(100) not null, 
	estado char(2) not null
)

go

create table TB_ENDERECO(
	id int identity(1,1) primary key,
	rua varchar(255) not null,
	bairro varchar(50) not null, 
	numero varchar(50) not null, 
	id_cidade int not null references TB_CIDADE(id),
	complemento varchar(100) not null,
	cep varchar(10) not null,
	tipo_endereco int not null
)

go

CREATE TABLE TB_CLIENTE_ENDERECO(
	id int identity(1,1) primary key, 
	tb_cliente_id int references TB_CLIENTE(id),
	tb_endereco_id int references TB_ENDERECO(id)
)

go 


insert into TB_CIDADE VALUES('Cajamar','SP')

